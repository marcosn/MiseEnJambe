package inf234;

import org.junit.Test;

import static org.junit.Assert.*;

public class MiseEnJambeTest {

    @Test
    public void testContient1() throws Exception {
        MiseEnJambe m1 = new MiseEnJambe("<a>xyz<b>uvw</b>toto</a>");
        assertTrue(m1.contient("uvw</"));
    }
    
    public void testContientContenu1(String c) throws Exception {
        MiseEnJambe s1 = new MiseEnJambe("<s>balise</s>");
        assertTrue(s1.contientContenu("is"));
    }

    public void testContientContenu2(String c) throws Exception {
        MiseEnJambe s2 = new MiseEnJambe("<t>balise</t>");
        assertTrue(s2.contientContenu("is"));
    }
    
    public void testContientContenu3(String c) throws Exception {
        MiseEnJambe s3 = new MiseEnJambe("<s>bali5e</s>");
        assertTrue(!(s3.contientContenu("is")));
    }

    public void testContientContenu4(String c) throws Exception {
        MiseEnJambe s4 = new MiseEnJambe("<t>bali5e</t>");
        assertTrue(!(s4.contientContenu("is")));
    }

    public void testContientContenu13(String c) throws Exception {
        MiseEnJambe s4 = new MiseEnJambe("<t attr=bali5e t>");
        assertTrue(!(s4.contientContenu("is")));
    }

    public void testContientContenu14(String c) throws Exception {
        MiseEnJambe s4 = new MiseEnJambe("<t <s> attr=bali5e </s> /t>");
        assertTrue(!(s4.contientContenu("is")));
    }

    public void testContientContenu5(String c) throws Exception {
        MiseEnJambe s5 = new MiseEnJambe("");
        assertTrue(!(s5.contientContenu("is")));
    }

    public void testContientContenu6(String c) throws Exception {
        MiseEnJambe s6 = new MiseEnJambe("");
        assertTrue(s6.contientContenu(""));
    }

    public void testSansCommentaire1(){
        MiseEnJambe c1 = new MiseEnJambe("<!--Commentaire début --><t>balise<t>");
        assertTrue((c1.sansCommentaire()) =="<t>balise</t>");
    }

    public void testSansCommentaire2(){
        MiseEnJambe c2 = new MiseEnJambe("<t>balise</t><!--Commentaire fin-->");
        assertTrue ((c2.sansCommentaire()) == "<t>balise</t>");
    }

    public void testSansCommentaire3(){
        MiseEnJambe c3 = new MiseEnJambe("<!--Commentaire début --><t>balise<t><!--Commentaire fin-->");
        assertTrue((c3.sansCommentaire()) == "<t>balise<t>");
    }

    public void testSansCommentaire4(){
        MiseEnJambe c4 = new MiseEnJambe("<t>texte balise début<t><!--Commentaire milieu--><t>texte balise début<t>");        
        assertTrue((c4.sansCommentaire()) == "<t>balise<t>");
    }

    public void testSansCommentaire5(){
        MiseEnJambe c5 = new MiseEnJambe("<!--Commentaire début --><t>texte balise début<t><!--Commentaire milieu--><t>texte balise début<t><!--Commentaire fin-->");        
        assertTrue((c5.sansCommentaire()) == "<t>balise<t>");
    }

    public void testSansCommentaire6(){
        MiseEnJambe c6 = new MiseEnJambe("");        
        assertTrue((c6.sansCommentaire()) == c6);
    }

    public void testSansCommentaire7(){
        MiseEnJambe c7 = new MiseEnJambe("<!--Commentaire début --><t><!-texte balise-> début<t><!--Commentaire milieu--><t>texte balise début<t><!--Commentaire fin-->");        
        assertTrue((c7.sansCommentaire()) == "<t><!-texte balise-> début<t>");
    }

    public void testFiltrer1(){
        MiseEnJambe f1 = new MiseEnJambe("<a>x<b>y</b>z</a>");
        assertTrue((f1.filter("a")) == "");
    }

    public void testFiltrer2(){
        MiseEnJambe f2 = new MiseEnJambe("<a>w<b>x<c>y</c></b>z</a>");
        assertTrue((f2.filtrer("a")) == "<a>wz</a>");
    }

    public void testFiltrer3(){
        MiseEnJambe f3 = new MiseEnJambe("");
        assertTrue((f3.filtrer("a")) == "");
    }

    public void testFiltrer4(){
        MiseEnJambe f4 = new MiseEnJambe("<a>w<b>x<c>y</c></b>z</a>");
        assertTrue((f4.filtrer("")) == c6);
    }

    public void testFiltrer5(){
        MiseEnJambe f5 = new MiseEnJambe("<a attr=w<b>x<c>y</c></b>z /a>");
        assertTrue((f5.filtrer("a")) == "w<b>x<c>y</c></b>z");
    }

    public void testFiltrer6(){
        MiseEnJambe f6 = new MiseEnJambe("<a attr=w<b>x<c>y</c></b>z /a>");
        assertTrue((f6.filtrer("b")) == "<a attr=wz /a>");
    }

    public void testFiltrer7(){
        MiseEnJambe f7 = new MiseEnJambe("<a attr=w<b>x<c>y</c></b>z /a>");
        assertTrue((f7.filtrer("b")) == f7);
    }

    public void testContientBalise1(){
        MiseEnJambe b1 = new MiseEnJambe("<s attr= /s>");
        assertTrue(b1.contientBalise("s"));
    }

    public void testContientBalise2(){
        MiseEnJambe b2 = new MiseEnJambe("<s attr= /s>");
        assertTrue(!(b2.contientBalise("a")));
    }

    public void testContientBalise3(){
        MiseEnJambe b3 = new MiseEnJambe("<s attr=<t>test balise</t> /s");
        assertTrue(!(b3.contientBalise("t")));
    }

}

